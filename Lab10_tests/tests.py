from __future__ import absolute_import

from unittest import TestCase
from ddt import ddt, data, file_data, unpack

from Lab10 import SolverLab10

@ddt
class Lab10TestCases(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    @file_data("concatenaciones-mock-data.json")
    @unpack
    def test_concatenation(self,entries):
        self.assertEqual(SolverLab10.concatenation(entries[0],entries[1]),entries[2])

    @file_data("standard_deviation-mock-data.json")
    @unpack
    def test_standar_deviation(self,entries):
        self.assertEqual(SolverLab10.standar_deviation(entries[0]),entries[1])

    @file_data("sort-mock-data.json")
    @unpack
    def test_sort(self, entries):
        self.assertEqual(SolverLab10.sort(entries[0]), entries[1])