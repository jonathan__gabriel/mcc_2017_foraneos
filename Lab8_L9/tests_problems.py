"""
Definicion de funciones para el problema 1 de la lectura 9
"""


def _population_mean(data_lista):
    ret_val = None
    if data_lista and isinstance(data_lista, list):
        suma = 0.0
        for itm in data_lista:
            suma += itm
        pm = suma / len(data_lista)
        ret_val = pm
    else:
        print "El parametro dato no es una lista o es una lista vacia"
    return ret_val


def standard_deviation(data_lista):
    ret_val = None
    if data_lista and isinstance(data_lista, list):
        promedio = _population_mean(data_lista)
        varianza = sum([(itm - promedio) ** 2 for itm in data_lista]) / len(data_lista)
        stdev = varianza ** 0.5
        ret_val = stdev
    else:
        print "El parametro dato no es una lista o es una lista vacia"
    return ret_val


def concatenar(palabra1, palabra2):
    ret_val = palabra1 + palabra2
    return ret_val

def sortList(lista_a):
    ret_val = sorted(lista_a)
    return ret_val