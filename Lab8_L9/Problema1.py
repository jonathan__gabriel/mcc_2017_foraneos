# -*- coding: utf-8 -*-
from __future__ import absolute_import
import unittest
import numpy
from ddt import ddt, file_data
from tests_problems import concatenar, standard_deviation, sortList

@ddt
class FooTestCase(unittest.TestCase):
    @file_data('listadesordenada.json')
    def test_sortlist(self, value):
        #print sortList(value)
        self.assertEqual(sortList(value)[0], min(value))
        self.assertEqual(sortList(value)[-1], max(value))

    @file_data('milpalabras.json')
    def test_concatenar2(self, value):
        self.assertEqual(concatenar(value[0], value[1]), value[0] + value[1])

    @file_data('sample_std_numbers.json')
    def test_standard_deviation(self, value):
        # Golden desviacion estandar
        arr = numpy.array(value)
        #print standard_deviation(value), numpy.std(arr)
        self.assertAlmostEqual(standard_deviation(value), numpy.std(arr), 5)



if __name__ == "__main__":
    unittest.main()
