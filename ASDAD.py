
from __future__ import absolute_import

from Lab10 import SolverLab10
import json
import random

totalResult = []
for i in range(2000):
    arr = [int(1000 * random.random()) for i in xrange(20)]
    result = SolverLab10.standar_deviation(arr)
    totalResult.append([arr,result])


with open("standard_deviation-mock-data.json", 'wb') as outfile:
    json.dump(totalResult, outfile)

