"""

"""
import sys as _sys
import os as _os
import auxiliary  # Para propositos de obtener una referencia del path

file_location = _os.path.join(auxiliary.__file__)
#  Se cambia el working directory a mcc_2017_foraneos y se puedan hacer imports
root_working_dir = _os.path.dirname(_os.path.dirname(file_location))
_sys.path.append(root_working_dir)

#import Lab3_L4.Problem9.test_problema

from Lab3_L4.Problem7.ListDuplicates import remove_duplicates_by_set, remove_duplicates_by_loop
from Lab3_L4.Problem9.StatisticalOperations import StatisticalOperations as sto
from Lab1_L2.Problem1.odd_or_even import testEven, testOdd, whatIs
from Lab3_L4.Problem10.BinarySearch import isFound
from Lab2_L3.Problem4.Problem4 import common_element_in_files as commom_numbers,  convert_file_to_list as leearchivo
from Lab1_L2.Problem2.list_confusion import Prueba1
from Lab1_L2.Problem3.palindromos import palindromos
from Lab2_L3.Problem5.Problem5 import palabra_reves

# Prueba No.1
def pruebaDuplicadosSet():
    resultado = remove_duplicates_by_set([1, 2, 3, 3, 4, 4, 4, 5, 6])
    if resultado == [1, 2, 3, 4, 5, 6]:
        print "Paso prueba de duplicados por set: {0}".format(resultado)
    else:
        print "Fallo prueba de duplicados por set: {0}".format(resultado)

# Prueba No.2
def pruebaDuplicadosLoop():
    resultado = remove_duplicates_by_loop([1, 2, 3, 3, 4, 4, 4, 5, 6])
    if resultado == [1, 2, 3, 4, 5, 6]:
        print "Paso prueba de duplicados por loop: {0}".format(resultado)
    else:
        print "Fallo prueba de duplicados por loop: {0}".format(resultado)

# Prueba No.3 - Problema 9 Operaciones stadisticas
def pruebaCuartillas():
    operations = sto()
    resultado = operations.quartiles([19, 21, 21, 24, 24, 24, 25, 28, 30])
    if resultado == (21.0, 26.5):
        print "Paso prueba de quartiles: {0}".format(resultado)
    else:
        print "Fallo prueba de quartiles: {0}".format(resultado)

#Prueba No.4 Problema 1 Odd or Even
def pruebaEvenOrOdd():
    print "is 18 Even: ", testEven(18)
    print "is 3 Odd: 3 ", testOdd(3)
    print "is -3 Even:  ", testEven(-3)
    print "is -3 Odd:  ", testOdd(-3)
    print "10 ", whatIs(10)
    print "-3.234 ", whatIs(-3.234)
    print "10.321 ", whatIs(10.321)
#Prueba No.4 Problema 10 BinarySearch
def pruebaBinarySearch():
    data = [1, 2, 3, 5, 8, 10]
    target = 3
    print 'Number ', target, ' is found in ', data, ' ? -->', isFound(data, target)


    data = [-10,-5.9,0,1, 2, 3, 5, 8, 800]
    target = -5.9
    print 'Number ', target, ' is found in ', data, ' ? -->', isFound(data, target)


    data = [2, 4, 5, 7, 8, 9, 12, 14, 17, 19, 22, 25, 27, 28, 33, 37, 40]
    print 'Number ', target, ' is found in ', data, ' ? -->', isFound(data, target)

#Prueba No.5 Problema 4
def pruebaNumerosComunes():
    happyfile=_os.path.join(root_working_dir,r"Lab2_L3/Problem4/happynumbers.txt")
    primesfile = _os.path.join(root_working_dir, r"Lab2_L3/Problem4/primenumbers.txt")
    outsfile = _os.path.join(root_working_dir, r"Lab2_L3/Problem4/output.txt")
    commom_numbers(happyfile, primesfile, outsfile)
    resultado=leearchivo(outsfile)
    if resultado== [7,13,19,23,31,79,97,103,109,139,167,193,239,263,293,313,331,367,
                    379,383,397,409,487,563,617,653,673,683,709,739,761,863,881,907,
                    937]:
        print 'paso la prueba numeros comunes en archivos happy numbers y primos'
    else:
        print 'NO paso la prueba happy number y primos'


def PruebaProblem2():
    a = [1, 1, 2, 3, "x", 8, 13, 21, 34, 55, 89, "z"]
    b = ["x", 1, 2, "y", 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, "mm", "z", 12000]
    if(Prueba1(a,b) == True):
        print 'Lab1_L2 - Problem2: Paso Prueba de listas'


def PruebaProblem3():
    bandera = palindromos('anita lava la tina')
    if bandera:
        print 'Lab1_L2 - Problem3: Pasa la prueba del palindromo'
    else:
        print 'Lab1_L2 - Problem3: Pasa la prueba del palindromo, no es palindromo'

def pruebaPalabraReves():
    palabra_reves("ola ke ace")
    palabra_reves("")
    palabra_reves()
    palabra_reves("-234234234")
    palabra_reves("me saludas a la tuya")


pruebaDuplicadosSet()
pruebaDuplicadosLoop()
pruebaCuartillas()
pruebaEvenOrOdd()
pruebaBinarySearch()
pruebaNumerosComunes()
PruebaProblem3()
PruebaProblem2()
pruebaPalabraReves()