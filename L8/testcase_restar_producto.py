# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Testcase2(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://automationpractice.com/index.php"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_case2(self):
        driver = self.driver
        driver.get(self.base_url + "index.php")
        driver.find_element_by_id("search_query_top").clear()
        driver.find_element_by_id("search_query_top").send_keys("dress")
        driver.find_element_by_name("submit_search").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseMoveAt | //div[@id='center_column']/ul/li/div/div[2]/div[2]/a/span | ]]
        driver.find_element_by_xpath("//div[@id='center_column']/ul/li/div/div[2]/div[2]/a/span").click()
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/span/span").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseMoveAt | //div[@id='center_column']/ul/li/div/div[2]/div[2]/a/span | ]]
        driver.find_element_by_xpath("//div[@id='center_column']/ul/li/div/div[2]/div[2]/a/span").click()
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/a/span").click()
        try: self.assertEqual("$57.96", driver.find_element_by_id("total_product_price_5_19_0").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("2", driver.find_element_by_name("quantity_5_19_0_0").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("#cart_quantity_down_5_19_0_0 > span").click()
        try: self.assertEqual("$28.98", driver.find_element_by_id("total_product_price_5_19_0").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("1", driver.find_element_by_name("quantity_5_19_0_0").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("#cart_quantity_down_5_19_0_0 > span").click()
        try: self.assertEqual("Your shopping cart is empty.", driver.find_element_by_css_selector("p.alert.alert-warning").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
