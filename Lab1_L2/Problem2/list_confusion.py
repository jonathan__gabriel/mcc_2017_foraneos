"""

"""
import random


def check_list_are_equal(list_a, list_b):
    """
    Regresa Verdadero en caso de que las listas sean del mismo tamano.
    Caso contrario regresa Falso.
    :param list_a: contiene una lista.
    :param list_b: contiene una lista.
    :return: True o False.
    """

    if len(list_a) != len(list_b):
        return False

    for i in range(0, len(list_a)):
        if list_a[i] != list_b[i]:
            return False

    return True

def find_common_elements(list_a, list_b):
    """

    Regresa una lista de los elementos que son comunes entre ambas listas

    :param list_a: contiene una lista.
    :param list_b: contiene una lista.
    :return: list.
    """
    return list( set(list_a) & set(list_b) )

def programa_inicia():
    """

    Inicia el programa principal del Problem2.
    :return: No tiene valor de retorno
    """

    # Pruebas - DefiniciOn de listas estAticas
    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    print "lista a ", a
    print "lista b", b

    elementos_comunes =  find_common_elements(a, b)

    print "Lista de elementos comunes en a y b = ", find_common_elements(a, b)

    assert check_list_are_equal(elementos_comunes, [1, 2, 3, 5, 8, 13])


    a = [1, 1, 2, 3, "x", 8, 13, 21, 34, 55, 89, "z"]
    b = ["x", 1, 2, "y", 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, "mm", "z", 12000]
    print "lista a ", a
    print "lista b", b

    elementos_comunes = find_common_elements(a, b)

    print "Lista de elementos comunes = ", elementos_comunes

    assert check_list_are_equal(elementos_comunes, [1, 2, 8, 13, 'x', 'z'])


    a = [1, 2, 3]
    b = [4, 5, 6]

    # GeneraciOn de listas random
    max_longitud_a = random.randint(0,200)      # lista a
    max_longitud_b = random.randint(0,200)      # lista b


    a = [random.randint(0,1000) for r in xrange(max_longitud_a)]
    b = [random.randint(0,1000) for r in xrange(max_longitud_b)]
    print "Lista de elementos comunes = ", find_common_elements(a, b)

    #Prueba 3

    a = []
    b = []

    elementos_comunes =  find_common_elements(a, b)
    print "listas vacias a y b = ", elementos_comunes
    assert check_list_are_equal(elementos_comunes,[])



def Prueba1(list_a, list_b):
    """
    Funcion que se usa para los TestCases de Jenkins

    :param list_a: contiene una lista.
    :param list_b: contiene una lista.
    :return: devuelve True o False.
    """
    elementos_comunes = find_common_elements(list_a, list_b)
    if (elementos_comunes is None):
        return False
    else:
        return True

