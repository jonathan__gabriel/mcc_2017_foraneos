"""

"""
import sys


def _ask_number(question):
    """
    Function to process and get input from keyboard

    String _ask_number(String)

    """

    #answer = raw_input(question) Cambio hecho por Ricardo Olvera para cachar los flotantes
    try:                                        # try - catch statement to process wrong input
        answer = raw_input(question)            # get input from keyboard
        ret_val = int(answer)                   # parsing input to int, if parsing fails, exception is raised
    except ValueError:
        print "Your input maybe was not a number!, you have to enter a non float number or character, exiting now..."
        return
    return ret_val

def start_program(numIn = 10):

    num = numIn
    """
    String start_program(int)
    Main function
    """
    myString = "Exception"

    #print "Welcome to odd or even program!"
    #num = _ask_number('Enter any number: ')     # call of _ask_number function

    if num is not None and isInt(num) == True:                         # if input is not null, validations are executed
        if num%2 == 0:
            if num % 4 == 0:
                #print "The number {0} is multiple of 4\n".format(num)
                sys.stdout.write("")
            else:
                #print "The number {0} is even\n".format(num)
                sys.stdout.write("")
            myString = "even"
        else:
            #print "The number {0} is odd\n".format(num)
            sys.stdout.write("")
            myString = "odd"
        #check = _ask_number('Enter another number to check if it divides evenly the 14first number (provided by you): ')
        check = num
        if check is not None:
            if num%check == 0:
                #print "The number {0} is divided evenly by {1}\n".format(num, check)
                sys.stdout.write("")
            else:
                #print "The number {0} is NOT divided evenly by {1}\n".format(num, check)
                sys.stdout.write("")
        #example = _ask_number('Enter a float number or a character: ')
        example = num
    return myString

def isInt(val):
    """
    Function validating int

    boolean isinstance(obj, int)

    """
    return isinstance(val, int)



def testEven(val):
    if(start_program(val) == "even"):
        return True
    else:
        return False

def testOdd(val):
    if(start_program(val) == "odd"):
        return True
    else:
        return False

def whatIs(val):
    return start_program(val)







