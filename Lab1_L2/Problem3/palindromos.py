"""
Problema 4: En este programa se introduce un string que contiene una palabra que bien puede o no ser palindromo.
El programa determina si la palabra ingresada cumple con los requisitos para ser un palindromo (La frase sea la misma
si se lee de izquierda a derecha o viceversa).
"""

#Inicio de la funcion palindromo

def palindromos(entrada_string):
    """

    Palindromos recibe un string y regresa
    un boolean si el string es palindromo

    :param entrada_string:
    :return Bool:

    """
    #validacion de entra sea tipo string

    if entrada_string == None:
        return False

    if not (isinstance(entrada_string, str) or isinstance(entrada_string, unicode)):
        print "Parametro debe ser de tipo string, saliendo del programa!!!"
        return False
    #validacion si es palindromo par o impar
    num = len(entrada_string)
    if num % 2 == 0:
        iter_indices = range(num/2)  # palindromos pares
    else:
        iter_indices = range((num-1)/2) # palindromos impares
    for ind in iter_indices:
        #print entrada_string[ind], entrada_string[-(ind+1)]
        if entrada_string[ind] != entrada_string[-(ind+1)]:
            print "La palabra \"{0}\" NO es palindromo".format(entrada_string)
            return False
    print "La palabra \"{0}\" es palindromo".format(entrada_string)
    return True
