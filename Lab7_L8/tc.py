# -*- coding: utf-8 -*-
from __future__ import absolute_import
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

from pyvirtualdisplay import Display


class Tc(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()

        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://automationpractice.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
        self.wait = WebDriverWait(self.driver, 100)

    def test_tc(self):
        driver = self.driver
        wait = self.wait
        twait = 2
        # Steps
        driver.get(self.base_url + "/index.php")
        while not self.is_element_present(By.XPATH, "//header[@id='header']/div[3]"):
            pass
        driver.find_element_by_xpath("//header[@id='header']/div[3]").click()
        while not self.is_element_present(By.XPATH, "//img[@alt='Printed Dress']"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//img[@alt='Printed Dress']").click()
        while not self.is_element_present(By.NAME, "Submit"):
            pass
        driver.find_element_by_name("Submit").click()
        while not self.is_element_present(By.XPATH, "//div[@id='layer_cart']/div/div[2]/div[4]/span/span"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/span/span").click()
        while not self.is_element_present(By.NAME, "Submit"):
            pass
        time.sleep(twait)
        driver.find_element_by_name("Submit").click()
        while not self.is_element_present(By.XPATH, "//div[@id='layer_cart']/div/div[2]/div[4]/span/span"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/span/span").click()
        while not self.is_element_present(By.NAME, "Submit"):
            pass
        time.sleep(twait)
        driver.find_element_by_name("Submit").click()
        while not self.is_element_present(By.XPATH, "//div[@id='layer_cart']/div/div[2]/div[4]/span/span"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/span/span").click()
        while not self.is_element_present(By.NAME, "Submit"):
            pass
        time.sleep(twait)
        driver.find_element_by_name("Submit").click()
        while not self.is_element_present(By.XPATH, "//div[@id='layer_cart']/div/div[2]/div[4]/span/span"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/span/span").click()
        while not self.is_element_present(By.NAME, "Submit"):
            pass
        time.sleep(twait)
        driver.find_element_by_name("Submit").click()
        while not self.is_element_present(By.XPATH, "//div[@id='layer_cart']/div/div[2]/div[4]/a/span"):
            pass
        time.sleep(twait)
        driver.find_element_by_xpath("//div[@id='layer_cart']/div/div[2]/div[4]/a/span").click()
        while not self.is_element_present(By.ID, "summary_products_quantity"):
            pass
        time.sleep(twait)
        try:
            self.assertEqual("5 Products", driver.find_element_by_id("summary_products_quantity").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        wait.until(lambda driver: driver.find_element_by_css_selector("div.columns-container"))
        # time.sleep(twait+1)
        # driver.find_element_by_css_selector("div.columns-container").click()
        time.sleep(twait)
        wait.until(lambda driver: driver.find_element_by_css_selector("i.icon-trash"))
        time.sleep(twait)
        driver.find_element_by_css_selector("i.icon-trash").click()
        time.sleep(twait)
        # end of Steps

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
        self.display.stop()


if __name__ == "__main__":
    unittest.main()
