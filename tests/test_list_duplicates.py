from __future__ import absolute_import

from unittest import TestCase
import unittest

from Lab3_L4.Problem7.ListDuplicates import remove_duplicates_by_loop
from Lab3_L4.Problem7.ListDuplicates import remove_duplicates_by_set

class Test_List_Duplicates(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_remove_common_elements_loop(self):
        a = [1, 2, 3, 3, 4, 4, 4, 5, 6]
        self.assertEqual(remove_duplicates_by_loop(a), [1,2,3,4,5,6])

    def test_remove_common_elements_set(self):
        b = ['a', 'a', 'b', 'b', 'c']
        self.assertEqual(remove_duplicates_by_set(b), ['a','b','c'])

    def test_remove_common_elements_loop_none(self):
        c = []
        self.assertEqual(remove_duplicates_by_loop(c), [])


if __name__ == '__main__':
    unittest.main()