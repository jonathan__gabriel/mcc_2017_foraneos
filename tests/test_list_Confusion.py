from __future__ import absolute_import
from unittest import TestCase
import unittest

from Lab1_L2.Problem2.list_confusion import find_common_elements
from Lab1_L2.Problem2.list_confusion import check_list_are_equal

class TestList_Confusion(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_find_common_elements_only_int(self):
        a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
        b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

        elementos_comunes = find_common_elements(a, b)
        self.assertTrue(check_list_are_equal(elementos_comunes, [1, 2, 3, 5, 8, 13]))

    def test_find_common_elements(self):
        a = [1, 1, 2, 3, "x", 8, 13, 21, 34, 55, 89, "z"]
        b = ["x", 1, 2, "y", 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, "mm", "z", 12000]

        elementos_comunes = find_common_elements(a, b)

        self.assertTrue(check_list_are_equal(elementos_comunes, [1, 2, 8, 13, 'x', 'z']))

    def test_find_common_elements_empty_arrays(self):
        self.assertFalse(find_common_elements([], []))

    def test_find_common_elements_no_array(self):
        self.assertRaises(Exception, find_common_elements, None, None)

    def test_find_common_elements_one_empty_array(self):
        a = [1, 1, 2, 3, "x", 8, 13, 21, 34, 55, 89, "z"]
        self.assertFalse(find_common_elements(a, []))

    def test_two_empty_arrays_are_equal(self):
        self.assertTrue(check_list_are_equal([],[]))

    def test_two_arrays_are_equal(self):
        self.assertTrue(check_list_are_equal([1,2,3],[1,2,3]))
        self.assertTrue(check_list_are_equal(['x', 'y', 'z'], ['x', 'y', 'z']))

    def test_two_arrays_same_length_are_not_equal(self):
        self.assertFalse(check_list_are_equal([1, 3, 2], [1, 2, 3]))
        self.assertFalse(check_list_are_equal([2, 3, 2], [1, 2, 5]))
        self.assertFalse(check_list_are_equal(['x', 'z', 'y'], ['x', 'y', 'z']))

    def test_two_arrays_different_length_are_not_equal(self):
        self.assertFalse(check_list_are_equal([1,2,3,4],[1,2,3]))
        self.assertFalse(check_list_are_equal([1], []))
        self.assertFalse(check_list_are_equal(['x', 'y'], ['o']))

if __name__ == '__main__':
    unittest.main()