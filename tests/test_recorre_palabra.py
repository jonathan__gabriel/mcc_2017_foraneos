from __future__ import absolute_import
from unittest import TestCase
import unittest
import __builtin__
from Lab2_L3.Problem5.Problem5 import _leer_palabra
from Lab2_L3.Problem5.Problem5 import reverse_string_by_word


class Test_recorre_palabra(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_leer_palabra(self):

        stringValue = 'batman el caballero'

        original_raw_input = __builtin__.raw_input
        __builtin__.raw_input = lambda _: stringValue
        self.assertEqual(_leer_palabra(), stringValue)
        __builtin__.raw_input = original_raw_input

    def test_reverse_string_by_word(self):
        self.assertEqual(reverse_string_by_word("batman el caballero"),"caballero el batman")

    def test_reverse_string_by_word(self):
        self.assertEqual(reverse_string_by_word("batman"),"batman")

    def test_reverse_string_by_word_empty_string(self):
        self.assertEqual(reverse_string_by_word(""),"")

    def test_reverse_string_with_None(self):
        self.assertRaises(Exception, reverse_string_by_word,None)

if __name__ == '__main__':
    unittest.main()