from __future__ import absolute_import
from unittest import TestCase
import  unittest
from Lab2_L3.Problem6.Problem6 import cows_and_bulls
from Lab2_L3.Problem6.Problem6 import cows_and_bulls_no_numeric

class TestCowsAndBullets(TestCase):
    """
    Test case of the problem and game Cows and Bullets. Include 6 tests that check the integrity of the program maintains good.
    """

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"


    def test_All_Cows(self):
        """
        Check that the input number should be equals to the expected number
        :return:
        """
        self.assertEqual(cows_and_bulls("1234","1234"),(4,0))

    def test_All_Bullets(self):
        """
        Check that the input number should have all the numbers of the expected number but not in the same position
        :return:
        """
        self.assertEqual(cows_and_bulls("1234","4321"),(0,4))

    def test_All_Half(self):
        """
        Check that the input number should have 2 numbers in the same position as the expected number and the other 2 on other position
        :return:
        """
        self.assertEqual(cows_and_bulls("1234","1243"),(2,2))

    def test_Empty_Case(self):
        """
        Check that with no input number and no expected number, the relation withstand this behavior
        :return:
        """
        self.assertEqual(cows_and_bulls("",""),(0,0))

    def test_No_Numeric(self):
        """
        Verify that the input is not a number
        :return:
        """
        self.assertFalse(cows_and_bulls_no_numeric("asd","1234"))

    def test_Numeric(self):
        """
        Verify that the input is a number
        :return:
        """
        self.assertTrue(cows_and_bulls_no_numeric("12","1234"))


if __name__ == '__main__':
    unittest.main()