from __future__ import absolute_import
from unittest import TestCase
import unittest

from Lab3_L4.Problem8.MatrixOperation import *


class TestStatisticalOperations(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_determinant_tres(self):
        matrix = [[1, -2, 3], [0, -3, -4], [0, 0, -3]]
        self.assertEqual(determinant(matrix), 9)

    def test_determinant_dos(self):
        matrix = [[1, 2], [3, 4]]
        self.assertEqual(determinant(matrix), -2)


if __name__ == '__main__':
    unittest.main()