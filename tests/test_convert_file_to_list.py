from __future__ import absolute_import
from unittest import TestCase
import unittest
import os as _os

from Lab2_L3.Problem4.Problem4 import *
from Lab1_L2.Problem2.list_confusion import check_list_are_equal


class TestConvert_file_to_list(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_convert_file_to_list(self):
        path_folder = _os.path.dirname(__file__)
        list_number = convert_file_to_list(_os.path.join(path_folder,"list_number_test.txt"))
        self.assertEqual(len(list_number),5)
        self.assertTrue(check_list_are_equal(list_number,[1, 7, 10, 13, 19]))

    def test_convert_None_file_to_list(self):
        list_number = convert_file_to_list(None)
        self.assertEqual(len(list_number),0)

    def test_convert_int_file_to_list(self):
        list_number = convert_file_to_list(5)
        self.assertEqual(len(list_number), 0)

    def test_common_element_in_files_output(self):
        path_folder = _os.path.dirname(__file__)
        hn_file = _os.path.join(path_folder,"happynumbers.txt")
        pn_file = _os.path.join(path_folder, "primenumbers.txt")
        out_file = _os.path.join(path_folder, "outfile_test.txt")
        self.assertTrue(common_element_in_files(hn_file, pn_file, out_file))

    def test_common_element_in_files_non_existant_param1(self):
        path_folder = _os.path.dirname(__file__)
        pn_file = _os.path.join(path_folder, "primenumbers.txt")
        out_file = _os.path.join(path_folder, "outfile_test.txt")
        self.assertFalse(common_element_in_files("noparam1.txt", pn_file, out_file))

    def test_common_element_in_files_non_existant_param2(self):
        path_folder = _os.path.dirname(__file__)
        hn_file = _os.path.join(path_folder, "happynumbers.txt")
        out_file = _os.path.join(path_folder, "outfile_test.txt")
        self.assertFalse(common_element_in_files(hn_file, "noparam2.txt", out_file))



if __name__ == '__main__':
    unittest.main()