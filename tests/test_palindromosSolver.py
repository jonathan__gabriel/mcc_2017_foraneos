from __future__ import absolute_import
import unittest
from Lab1_L2.Problem3.palindromos import palindromos

class TestPalindromo(unittest.TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_None_string(self):
        self.assertFalse(palindromos(None))

    def test_empty_string(self):
        self.assertTrue(palindromos(""))

    def test_string_impar(self):
        self.assertTrue(palindromos("abcd2dcba"))

    def test_string_par(self):
        self.assertTrue(palindromos("allivessevilla"))

    def test_string_1_length(self):
        self.assertTrue(palindromos("a"))

    def test_string_impar_is_not_palindrome(self):
        self.assertFalse(palindromos("aasaadaas"))

    def test_string_par_is_not_palindrome(self):
        self.assertFalse(palindromos("nnenrer"))

    def test_int_is_not_palindrome(self):
        self.assertFalse(palindromos(5))

if __name__ == '__main__':
    unittest.main()