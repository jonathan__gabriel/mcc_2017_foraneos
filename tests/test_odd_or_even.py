from __future__ import absolute_import
from unittest import TestCase
import  unittest
from Lab1_L2.Problem1.odd_or_even import isInt
from Lab1_L2.Problem1.odd_or_even import _ask_number
import __builtin__


class TestOddOrEven(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_isInt(self):
        self.assertTrue(isInt(1))
        self.assertTrue(isInt(0))
        self.assertTrue(isInt(-1))

        self.assertFalse(isInt("X"))

        self.assertFalse(isInt(None))

        self.assertFalse(isInt(-0.0))
        self.assertFalse(isInt(-1.0))
        self.assertFalse(isInt(1.0))

    def test_ask_number5(self):

        stringValue = "5"

        original_raw_input = __builtin__.raw_input
        __builtin__.raw_input = lambda _: stringValue

        self.assertTrue(_ask_number("Question"),5)

        __builtin__.raw_input = original_raw_input

    def test_ask_number_not_int(self):

        stringValue = "asd"

        original_raw_input = __builtin__.raw_input
        __builtin__.raw_input = lambda _: stringValue

        self.assertEqual(_ask_number("Question"),None)

        __builtin__.raw_input = original_raw_input

if __name__ == '__main__':
    unittest.main()