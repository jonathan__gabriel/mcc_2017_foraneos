from __future__ import absolute_import
from unittest import TestCase
import unittest
import os as _os

from Lab3_L4.Problem9.StatisticalOperations import StatisticalOperations as st_o
operaciones = st_o()

class TestStatisticalOperations(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_population_mean(self):
        self.assertEqual(operaciones.population_mean([1, 7, 10, 13, 19]), 10)

    def test_standard_deviation(self):
        self.assertAlmostEqual(operaciones.standard_deviation([0.8, 0.4, 1.2, 3.7, 2.6, 5.8]), 1.883628, places=5)

    def test_mode(self):
        self.assertEqual(operaciones.mode([1, 2, 3, 3]), 3)

    def test_quartiles(self):
        q1, q3 = operaciones.quartiles([19, 21, 21, 24, 24, 24, 25, 28, 30])
        self.assertEqual( (q1, q3), (21.0, 26.5))

    def test_common_element_in_files_non_existant_param1(self):
        dict_valores = operaciones.lineal_regression([43, 21, 25, 42, 57, 59], [99, 65, 79, 75, 87, 81])
        self.assertAlmostEqual(dict_valores['a'], 65.141571, places=5)
        self.assertAlmostEqual(dict_valores['b'], 0.385224, places=5)



if __name__ == '__main__':
    unittest.main()