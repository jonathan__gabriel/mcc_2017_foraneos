from __future__ import absolute_import
from unittest import TestCase
import unittest
from Lab3_L4.Problem10.BinarySearch import isFound


"""
Test cases for Binary Search
"""

#print 'Number ', target, ' is found in ', data, ' ? -->', binary_search(data, target, 0, len(data))

class TestBinarySearch(TestCase):

    def setUp(self):
        print "\n********************************************************"
        print "Unit Tested: {0}".format(self._testMethodName)
        print "********************************************************"

    def test_BinarySearch1(self):
        data = [1, 2, 3, 5, 8, 10]
        target = 3

        self.assertEqual(isFound(data, target), True)

    def test_BinarySearch2(self):
        data = [1, 2, 3, 5, 8, 10]
        target = 13
        self.assertEqual(isFound(data, target), 'Target > max(data)')

    def test_BinarySearch3(self):
        data = [1, 2, 3, 5, 8, 10]
        target = -2
        self.assertEqual(isFound(data, target), False)

    def test_BinarySearch4(self):
        data = [-12,-10,-3,0,1, 2, 3, 5, 8, 10]
        target = -20
        self.assertRaises(Exception, isFound(data, target), None)

    def test_BinarySearch5(self):
        data = [-9,-8,-3,0,1, 2, 3, 5, 8, 10]
        target = -9
        self.assertEqual(isFound(data, target), True)


    def test_BinarySearch6(self):
        data = [-9,-8,-3,0,1, 2, 3, 5, 8, 10]
        target = -200
        self.assertEqual(isFound(data, target), False)


    def test_BinarySearchException(self):
        data = [1, 2, 3, 5, 8, 10]
        target = 12
        self.assertRaises(Exception, isFound(data, target), None)



if __name__ == '__main__':
    unittest.main()