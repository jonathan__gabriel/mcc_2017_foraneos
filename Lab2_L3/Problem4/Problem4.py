"""

Problem 4. File

Given two .txt files that have lists of numbers in them, find the numbers that are overlapping.
One .txt file has a list of all prime numbers under 1000, and the other .txt file has a list of happy numbers
up to 1000. The output should be stored in a third file, named as output.txt.

(If you forgot, prime numbers are numbers that can't be divided by any other number. And yes, happy numbers
are a real thing in mathematics - you can look it up on Wikipedia. The explanation is easier with an example, which I
will describe below.)


"""
import os
from Lab1_L2.Problem2.list_confusion import find_common_elements


def convert_file_to_list(file_path):
    """
        Read the numbers in a text file
        and store the values in a list

    :param file_path:
    :return:
    """
    try:
        return [int(line.strip()) for line in open(file_path, 'r')]
    except:
        return []


def common_element_in_files(file1, file2, output_file):  # funcion validar elementos comunues y generar la salida
    fexists = os.path.isfile
    if not fexists(file1):   # validar que los archivos existan
        print "El archivo {0} no existe".format(file1)
        return False
    if not fexists(file2):
        print "El archivo {0} no existe".format(file2)
        return False
    list_a = convert_file_to_list(file1)     # crear una lista con los elementos del archivo
    list_b = convert_file_to_list(file2)

    common_elements = find_common_elements(list_a, list_b)
    # Ordenar los numeros comunes de la lista
    common_elements.sort()

    filewr = open(output_file, 'w')   # salida de numeros comunes
    for item in common_elements:
        print >> filewr, item
    filewr.close()
    if common_elements:
        return True
