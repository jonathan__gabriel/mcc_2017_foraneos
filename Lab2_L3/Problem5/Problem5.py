"""

Problem 5. Reverse Word

Write a program (using functions!) that asks the user for a long string containing multiple words.
Print back to the user the same string, except with the words in backwards order. For example, say I type the string:

  My name is Michele
Then I would see the string:

  Michele is name My


"""


def _leer_palabra():
    palabra = raw_input("Introduce un texto: ")
    return palabra

def reverse_string_by_word(rpalabra):
    split_list = str.split(rpalabra, ' ')
    split_list.reverse()

    return (' ').join(split_list)

def palabra_reves(palabra="Exception"):
    try:
        if palabra is None:
            palabra = _leer_palabra()
        reversed_words = reverse_string_by_word(palabra)

        print "ORIGINAL: ", palabra, " REVERSED: ", reversed_words
        #print "   Al reves: ", final
    except Exception:
        return "Exception"

    return reversed_words



#palabra_reves("ola ke ace")

#palabra_reves("primer test para probar el codigo")

#palabra_reves("esta es la segunda prueba")


# 2
#test1 = palabra_reves("abc d2 dc ba")

#print test1

#assert test1


"""
 xrange(stop)
xrange(start, stop[, step])

This function is very similar to range(), but returns an xrange object instead of a list. This is an opaque sequence
type which yields the same values as the corresponding list, without actually storing them all simultaneously.
The advantage of xrange() over range() is minimal (since xrange() still has to create the values when asked for them)
except when a very large range is used on a memory-starved machine or when all of the range's elements are never used
(such as when the loop is usually terminated with break)


reversed(seq)
Return a reverse iterator. seq must be an object which has a __reversed__() method or
supports the sequence protocol (the __len__() method and the __getitem__() method with integer
arguments starting at 0)


str.split([sep[, maxsplit]])
Return a list of the words in the string, using sep as the delimiter string.
If maxsplit is given, at most maxsplit splits are done (thus, the list will
have at most maxsplit+1 elements). If maxsplit is not specified or -1, then there
is no limit on the number of splits (all possible splits are mad

"""