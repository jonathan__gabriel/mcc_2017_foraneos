
class SolverLab10:

    @staticmethod
    def concatenation(string1,string2):
        if string1 is None or string2 is None:
            return None
        return string1 + string2

    @staticmethod
    def standar_deviation(list_integer):
        if(not list_integer):
            return 0

        longitud = len(list_integer)

        if( longitud == 0 ):
            return  0

        mean = sum(list_integer) / longitud
        ss = sum((x-mean)**2 for x in list_integer) / longitud
        return ss ** 0.5

    @staticmethod
    def quicksort(lst):
        if not lst or len(lst) == 0:
            return []
        return (SolverLab10.quicksort([x for x in lst[1:] if x < lst[0]])
                + [lst[0]] +
                SolverLab10.quicksort([x for x in lst[1:] if x >= lst[0]]))

    @staticmethod
    def sort(lst):
        return SolverLab10.quicksort(lst)
