"""

Problem 7. List Duplicates

Write a program (function!) that takes a list and returns a new list that contains all the elements of the first list minus all the duplicates.

Extras:

Write two different functions to do this - one using a loop and constructing a list, and another using sets.


"""



def remove_duplicates_by_loop(t):
    """
        returns a new list that contains all the elements of the first list minus all the duplicates by using loops
        and constructing a list.
        If t is None, the result is a empty list

        :param t: the list
        :return: a list
        """

    result = []

    if t is None:
        return result
    dictio = {}
    for e in t:
        element = dictio.get(e)
        if element:
            continue
        result.append(e)
        dictio[e] = True

    return result




def remove_duplicates_by_set(t):
    """
    returns a new list that contains all the elements of the first list minus all the duplicates by using sets.
    If t is None, the result is a empty list

    :param t: the list
    :return: a list
    """
    if t is None or not t:

        return []

    red=list(set(t))
    red.sort()
    return red

'''
# Se prueba sin ninguna lista
print remove_duplicates_by_set(None)
# Se prueba con una lista con elementos repetidos
print remove_duplicates_by_set([1, 2, 3, 3, 4, 4, 4, 5, 6])
# Se prueba con una lista con elementos repetidos
print remove_duplicates_by_set(['a', 'a', 'b', 'b', 'c'])
# Se prueba con una lista vacia
print remove_duplicates_by_loop([])
# Se prueba con una lista con elementos repetidos
print remove_duplicates_by_loop([1, 2, 3, 3, 4, 4, 4, 5, 6])
# Se prueba con una lista con elementos repetidos
print remove_duplicates_by_loop(['a', 'a', 'b', 'b', 'c'])
'''