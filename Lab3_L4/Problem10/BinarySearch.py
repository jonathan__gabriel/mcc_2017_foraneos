"""
Problem 10. Binary Search

Write a Python program for binary search.
Binary Search : In computer science, a binary search or half-interval search algorithm finds the position of a target
value within a sorted array. The binary search algorithm can be classified as a dichotomies divide-and-conquer search
algorithm and executes in logarithmic time.
Test Data :
binary_search([1,2,3,5,8], 6) -> False
binary_search([1,2,3,5,8], 5) -> True

"""

def binary_search(data, target, low, high):
    if target <= max(data):
        """
        Return True if target is found in indicated portion of a Python list.
        The search only considers the portion from data[low] to data[high] inclusive.

        input: list, int int int
        output: boolean
        """
        if low > high:
            return False
        else:
            mid = (low + high)/2
            #print mid
            if target == data[mid]:
                return True
            elif target < data[mid]:
                return binary_search(data, target, low, mid-1)
            else:
                # recur on the portion right of the middle
                return binary_search(data, target, mid + 1, high)
    else:
        return 'Target > max(data)'


def isFound(data, target):
    try:
        return binary_search(data, target, 0, len(data))
    except Exception:
        return "Exception"

data = [1, 2, 3, 5, 8, 10]
target = 3
#print 'Number ', target, ' is found in ', data, ' ? -->', binary_search(data, target, 0, len(data))


data = [1, 2, 3, 5, 8]
target = 8
#print 'Number ', target, ' is found in ', data, ' ? -->', binary_search(data, target, 0, len(data))


data = [2, 4, 5, 7, 8, 9, 12, 14, 17, 19, 22, 25, 27, 28, 33, 37, 40, 50]
#print 'Number ', target, ' is found in ', data, ' ? -->', binary_search(data, target, 0, len(data))

data = [2, 5]
target = 2
#print 'Number ', target, ' is found in ', data, ' ? -->', binary_search(data, target, 0, len(data))




#Jenkins
#Jenkins


#data = [2, 4, 5, 7, 8, 9, 12, 14, 17, 19, 22, 25, 27, 28, 33, 37, 40, 50]
#print binary_search(data, 40, min(data), max(data))

"""
Note:
For any index j, we know that all the values stored at indices
        0, . . . , j-1 are less than or equal to the value at index j,
and all the values stored at indices
        j + 1, . . . , n-1 are greater than or equal to that at index j.

The algorithm maintains two parameters, low and high, such that all the candidate
entries have index at least low and at most high. Initially, low = 0 and high = n-1.

We then compare the target value to the median candidate, that is, the item data[mid] with index
mid =  (low + high)/2  .

"""