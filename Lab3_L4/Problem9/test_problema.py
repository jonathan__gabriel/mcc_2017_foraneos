"""

"""


import os as _os
from read_csv_file import ReadCSVFile as ReadClass
from StatisticalOperations import StatisticalOperations as StOp


this_dir = _os.path.dirname(__file__)   # Se trae el path del directorio donde esta este archivo
filename = _os.path.join(this_dir, 'smallFile.csv')
csv_file = ReadClass(filename)
lista_small = csv_file.get_data()
filename = _os.path.join(this_dir, 'largeFile.csv')
csv_file = ReadClass(filename)
lista_large = csv_file.get_data()

operaciones = StOp()
print "Population Mean ={0}".format(operaciones.population_mean(lista_small))
print "Population Mean ={0}".format(operaciones.population_mean(lista_large))
print "Standard deviation ={0}".format(operaciones.standard_deviation(lista_small))
print "Standard deviation ={0}".format(operaciones.standard_deviation(lista_large))
print "Mode ={0}".format(operaciones.mode([1, 2, 3, 3]))
print "Mode ={0}".format(operaciones.mode(lista_small))
print "Mode ={0}".format(operaciones.mode(lista_large))
q1, q3 = operaciones.quartiles([19, 21, 21, 24, 24, 24, 25, 28, 30])
print "Q1 = {0}, Q3 = {1}".format(q1, q3)
q1, q3 = operaciones.quartiles(lista_small)
print "Q1 = {0}, Q3 = {1}".format(q1, q3)
q1, q3 = operaciones.quartiles(lista_large)
print "Q1 = {0}, Q3 = {1}".format(q1, q3)
dict_valores = operaciones.lineal_regression([43, 21, 25, 42, 57, 59], [99, 65, 79, 75, 87, 81])
print "a = {0} ; b = {1}".format(dict_valores['a'], dict_valores['b'])
