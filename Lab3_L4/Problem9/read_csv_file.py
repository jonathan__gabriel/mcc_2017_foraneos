"""

"""

import csv as _csv


class ReadCSVFile(object):
    def __init__(self, archivo):
        self._path_archivo = archivo

    def get_data(self):
        ret_list = []
        opened_file = None
        try:
            with open(self._path_archivo, 'rU') as csvfile:
                opened_file = csvfile
                data = _csv.reader(csvfile, delimiter=',')
                for row in data:
                    # Si la casilla esta vacia
                    if not row[0]:
                        continue
                    else:
                        ret_list.append(int(row[0]))
        except IOError, e:  # If file I/O errors, handle right away
            print e
        except _csv.Error, e:  # If csv error, handle right away
            print e
        finally:  # If csv file open, always close it before exiting from this function
            if opened_file is not None:
                opened_file.close()
            return ret_list
