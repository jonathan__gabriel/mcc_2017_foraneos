"""
Problem 9. Statistical Operations

Write a program (Methods in Classes!) that allows you analyze data sets coming from text files (CSV).
You have to implement functions (do not use math libraries) to analyze data such as:
Population Mean
Standard Deviation
Mode
Quartiles
Lineal Regression -- Return parameters
"""


class StatisticalOperations(object):
    def __init__(self):
        pass

    def population_mean(self, data_lista):
        ret_val = None
        if data_lista and isinstance(data_lista, list):
            suma = 0.0
            for itm in data_lista:
                suma += itm
            pm = suma / len(data_lista)
            ret_val = pm
        else:
            print "El parametro dato no es una lista o es una lista vacia"
        return ret_val

    def standard_deviation(self, data_lista):
        ret_val = None
        if data_lista and isinstance(data_lista, list):
            promedio = self.population_mean(data_lista)
            varianza = sum([(itm - promedio)**2 for itm in data_lista]) / len(data_lista)
            stdev = varianza ** 0.5
            ret_val = stdev
        else:
            print "El parametro dato no es una lista o es una lista vacia"
        return ret_val

    def mode(self, data_lista):
        ret_val = None
        if data_lista and isinstance(data_lista, list):
            ret_val = max(set(data_lista), key=data_lista.count)
        else:
            print "El parametro dato no es una lista o es una lista vacia"
        return ret_val

    def quartiles(self, data_lista):
        ret_val = None
        if data_lista and isinstance(data_lista, list):
            n = len(data_lista)
            data_lista.sort()
            elemn1 = ((n+1)/4.0) - 1  # Debido a que las listas empiezan con indice 0
            if elemn1 == int(elemn1):
                q1 = data_lista[int(elemn1)]
            else:
                q1 = self.population_mean([data_lista[int(elemn1)], data_lista[int(elemn1) + 1]])
            elemn2 = ((3.0*(n + 1)) / 4.0) - 1  # Debido a que las listas empiezan con indice 0
            if elemn2 == int(elemn2):
                q3 = data_lista[int(elemn2)]
            else:
                q3 = self.population_mean([data_lista[int(elemn2)], data_lista[int(elemn2) + 1]])
            ret_val = (q1, q3)
        else:
            print "El parametro dato no es una lista o es una lista vacia"
        return ret_val

    def lineal_regression(self, data_lista_x, data_lista_y):
        ret_val = None
        if data_lista_x and data_lista_y and isinstance(data_lista_x, list) and isinstance(data_lista_y, list) \
                and (len(data_lista_x) == len(data_lista_y)):
            sumx = sum(data_lista_x) / 1.0
            sumy = sum(data_lista_y) / 1.0
            sumxy = sum([data_lista_x[ele] * data_lista_y[ele] for ele in xrange(len(data_lista_x))]) / 1.0
            sumx2 = sum([data_lista_x[ele] * data_lista_x[ele] for ele in xrange(len(data_lista_x))]) / 1.0
            # sumy2 = sum([data_lista_y[ele] ** data_lista_y[ele] for ele in xrange(len(data_lista_y))])
            a = ((sumy*sumx2) - (sumx*sumxy))/((len(data_lista_x)*sumx2) - (sumx**2))
            b = ((len(data_lista_y)*sumxy) - (sumx*sumy)) / ((len(data_lista_x)*sumx2) - (sumx**2))
            print "returns coeficients a and b from the formula y'= a + bx"
            ret_val = {'a': a, 'b': b}
        else:
            print "Los parametro dato no son dos listas o son listas vacias o las listas no son del mismo tamano"
        return ret_val
