"""
Problem 8. Matrix Operation

Find the way to represent a matrix using lists.

Write a program (function!) that computes the determinant for matrix sizes: 1x1, 2x2, 3x3.


"""

import numpy as np


def printMatrix(matrix):
    if len(matrix) == 3:
        print "%i  %i  %i" % (matrix[0][0], matrix[0][1], matrix[0][2])
        print "%i  %i  %i" % (matrix[1][0], matrix[1][1], matrix[1][2])
        print "%i  %i  %i" % (matrix[2][0], matrix[2][1], matrix[2][2])
    if(len(matrix) == 2):
        print "%i  %i" % (matrix[0][0], matrix[0][1])
        print "%i  %i" % (matrix[1][0], matrix[1][1])


def determinant(matrix, mul=1):
    width = len(matrix)
    if width == 1:
        return mul * matrix[0][0]
    else:
        sign = -1
        total = 0
        for i in range(width):
            m = []
            for j in range(1, width):
                buff = []
                for k in range(width):
                    if k != i:
                        buff.append(matrix[j][k])
                m.append(buff)
            sign *= -1
            total += mul * determinant(m, sign * matrix[0][i])
        return total


'''
matrix = [[1, -2, 3], [0, -3, -4], [0, 0, -3]]
printMatrix(matrix)
print determinant(matrix)
print np.linalg.det(matrix)

matrix = [[1, 2], [3, 4]]
printMatrix(matrix)
print determinant(matrix)
print np.linalg.det(matrix)
'''


"""
Notes:

Determinant

|A| =   | a    b |
        | c    d | = ad - bc


|A| =   | a   b   c |
        | d   e   f |  =  a | e   f |  -  b  | d   f |   +   c | d   e |
        | g   h   i |       | h   i |        | g   i |         | g   h |

                        = aei + bfg + cdh - ceg - bdi - afh
"""